import React from 'react';
import EditableTable from './components/EditableTable';

const App = () => {
  return (
      <div>
        <h1>Drag and Drop Editable Table</h1>
        <EditableTable />
      </div>
  );
};

export default App;
