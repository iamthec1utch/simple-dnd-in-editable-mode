import React, { useState } from 'react';
import { useDrag, useDrop } from 'react-dnd';

const TableRow = ({ index, id, name, moveRow, onEdit, onSave }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [editedName, setEditedName] = useState(name);
    const [{ isDragging }, drag] = useDrag({
        type: 'TABLE_ROW',
        item: { index },
    });

    const [, drop] = useDrop({
        accept: 'TABLE_ROW',
        hover: (item) => {
            if (!dragRef.current) {
                return;
            }

            const dragIndex = item.index;
            const hoverIndex = index;

            if (dragIndex === hoverIndex) {
                return;
            }

            moveRow(dragIndex, hoverIndex);
            item.index = hoverIndex;
        },
    });

    const dragRef = React.useRef(null);
    drag(drop(dragRef));

    const handleEditClick = () => {
        onEdit(id);
        setIsEditing(true);
    };

    const handleSaveClick = () => {
        onSave({ id, name: editedName });
        setIsEditing(false);
    };

    const handleCancelClick = () => {
        setIsEditing(false);
        setEditedName(name);
    };

    const handleInput = (e) => {
        setEditedName(e.target.value);
    };

    return (
        <tr ref={dragRef} style={{ opacity: isDragging ? 0.5 : 1 }}>
            {isEditing ? (
                <>
                    <td>{id}</td>
                    <td>
                        <input type="text" value={editedName} onInput={handleInput} />
                    </td>
                    <td>
                        <button onClick={handleSaveClick}>Save</button>
                        <button onClick={handleCancelClick}>Cancel</button>
                    </td>
                </>
            ) : (
                <>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>
                        <button onClick={handleEditClick}>Edit</button>
                    </td>
                </>
            )}
        </tr>
    );
};

export default TableRow;
