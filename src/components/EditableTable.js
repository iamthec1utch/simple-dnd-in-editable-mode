import React, { useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import TableRow from './TableRow';

const EditableTable = () => {
    const [rows, setRows] = useState([
        { id: 1, name: 'Poly Ribina' },
        { id: 2, name: 'Jenya Savchenko' },
        { id: 3, name: 'Lyusi Ribkina' },
    ]);

    const moveRow = (dragIndex, hoverIndex) => {
        setRows((prevRows) => {
            const newRows = [...prevRows];
            const draggedRow = newRows[dragIndex];

            newRows.splice(dragIndex, 1);
            newRows.splice(hoverIndex, 0, draggedRow);

            return newRows;
        });
    };

    const handleEdit = (id) => {

    };

    const handleSave = (editedRow) => {
        // onSave принимает объект с актуальным id и name, что помогает избежать ошибку,
        // связанную с изменением порядка строк
        setRows((prevRows) =>
            prevRows.map((row) => (row.id === editedRow.id ? editedRow : row))
        );
    };

    return (
        <DndProvider backend={HTML5Backend}>
            <table>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                {rows.map((row, index) => (
                    <TableRow
                        key={row.id}
                        index={index}
                        id={row.id}
                        name={row.name}
                        moveRow={moveRow}
                        onEdit={handleEdit}
                        onSave={handleSave}
                    />
                ))}
                </tbody>
            </table>
        </DndProvider>
    );
};

export default EditableTable;
